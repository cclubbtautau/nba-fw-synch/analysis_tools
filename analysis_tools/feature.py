from analysis_tools.base_object import TaggedObject


class Systematic(TaggedObject):
    def __init__(self, name, expression, up="_up", down="_down", *args, **kwargs):
        self.expression = expression
        self.up = up
        self.down = down
        super(Systematic, self).__init__(name, *args, **kwargs)

    def __repr__(self):
        return "Systematic(name=%s, expression=%s)" % (self.name, self.expression)


class Feature(TaggedObject):
    def __init__(self, name, expression, *args, **kwargs):
        self.expression = expression
        self.selection = kwargs.pop("selection", None)
        self.binning = kwargs.pop("binning", (10, 0, 1))
        self.central = kwargs.pop("central", "")
        self.systematics = kwargs.pop("systematics", [])

        super(Feature, self).__init__(name, *args, **kwargs)

    def __repr__(self):
        return "Feature(name=%s, expression=%s)" % (self.name, self.expression)


